//Requireds 
const express = require('express')
const app = express()
const hbs = require("hbs");
require('./hbs/helpers')
    // Main 

const port = process.env.PORT || 3000;


app.use(express.static(__dirname + '/public'));

hbs.registerPartials(__dirname + '/views/parciales');


// Express hbs engine
app.set("view engine", "hbs");

app.get('/', function(req, res) {

    res.render('home', {
        nombre: 'LuIs Tejada'
    });
});

app.get("/about", function(req, res) {
    res.render("about", {
        titulo: "Imagen de un Carro"
    });
});

app.listen(port, () => {
    console.log(`Escuchando peticiones en el puerto ${port}`);
})

// End of Main